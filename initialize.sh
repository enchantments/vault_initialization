#!/bin/sh

VAULT_LOCAL_SECRETS_FILE=${1}

while :
do
  vault operator init -status
  STATUSCODE=$?
  
  if test $STATUSCODE -eq 2; then
    echo "Initializing Vault."
    vault operator init -format=json | tee ${VAULT_LOCAL_SECRETS_FILE}
    exit 0
  elif test $STATUSCODE -eq 0; then
    echo "Vault already initialized."
    exit 0
  else
    sleep 3
  fi
done
