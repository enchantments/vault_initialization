resource "null_resource" "initialize" {
  triggers = {
    vault_instances = join(" ", var.control_plane_ips)
    script          = file("${path.module}/initialize.sh")
  }

  provisioner "local-exec" {
    command = "${path.module}/initialize.sh ${var.vault_local_secrets_file}"

    environment = {
      VAULT_ADDR        = var.vault_address
      VAULT_CACERT      = var.vault_cacert_path
      VAULT_CLIENT_CERT = var.vault_client_cert_path
      VAULT_CLIENT_KEY  = var.vault_client_key_path
    }
  }
}
