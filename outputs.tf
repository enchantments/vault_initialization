output "initialization_id" {
    value = null_resource.initialize.id
}

output "unseal_id" {
    value = null_resource.unseal.*.id
}
