#!/bin/sh

VAULT_LOCAL_SECRETS_FILE=${1}

vault status 2>&1 > /dev/null
STATUSCODE=$?

if test $STATUSCODE -eq 2; then
    echo "Unsealing Vault."

    vault operator unseal $(jq -r .unseal_keys_b64[0] ${VAULT_LOCAL_SECRETS_FILE})
    vault operator unseal $(jq -r .unseal_keys_b64[1] ${VAULT_LOCAL_SECRETS_FILE})
    vault operator unseal $(jq -r .unseal_keys_b64[2] ${VAULT_LOCAL_SECRETS_FILE})
fi

rm ${VAULT_LOCAL_SECRETS_FILE}
sync
