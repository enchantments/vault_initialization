resource "null_resource" "unseal" {
  count = var.auto_unseal ? 0 : length(var.control_plane_ips)

  triggers = {
    id     = null_resource.initialize.id
    script = file("${path.module}/unseal.sh")
  }

  provisioner "local-exec" {
    command = "gcloud compute scp ${var.vault_local_secrets_file} ${var.control_plane_instance_names[count.index]}:${var.vault_local_secrets_file}"
  }

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = element(var.control_plane_ips, count.index)
    }

    source      = "${path.module}/unseal.sh"
    destination = "/tmp/unseal.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
      host        = element(var.control_plane_ips, count.index)
    }
    
    inline = [
      "chmod +x /tmp/unseal.sh",
      "/tmp/unseal.sh ${var.vault_local_secrets_file}",
      "rm -f /tmp/unseal.sh"
    ]
  }
}
