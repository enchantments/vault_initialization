variable "control_plane_ips" {
  default = []
}

variable "control_plane_instance_names" {
  default = []
}

variable "auto_unseal" { default = false }
variable "vault_address" {}
variable "vault_cacert_path" {}
variable "vault_client_cert_path" {}
variable "vault_client_key_path" {}
variable "ssh_user" {}
variable "ssh_private_key" {}
variable "vault_local_secrets_file" { default = "vault_initial_secrets.json" }
